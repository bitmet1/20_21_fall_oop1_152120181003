#include <cmath>
#include <iostream>
#include <ctime>

using namespace std;

int main()
{
	srand(time(NULL));
	int N, *A, *B;

	cin >> N;
	A = new int[N];
	B = new int[N];

	for (int i = 0; i < N; i++)
	{
		cin >> A[i];
	}

	for (int i = 0; i < N; i++)
	{
		B[N - i - 1] = A[i];
	}

	for (int i = 0; i < N; i++)
	{
		cout << B[i] << " ";
	}

	return 0;
}
