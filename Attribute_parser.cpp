#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

using namespace std;
int main()
{
	int N, Q , count ;
	cin >> N >> Q;
	cin.ignore();
	string *hrml , *inquer, *quer , *result , imp , temp;
	vector <string> tag;

	inquer = new string[N];
	result = new string[N];
	hrml = new string[N];
	quer = new string[Q];
	for (int i = 0; i < N; i++)
	{
		getline(cin, imp);
		hrml[i] = imp;
	}

	for (int j = 0; j < Q; j++)
	{
		getline(cin, imp);
		quer[j] = imp;
	}

	count = 0;
	for (int k = 0; k < N; k++)
	{
		stringstream ss;
		imp = hrml[k];

		imp.erase(remove(imp.begin(), imp.end(), '\"'), imp.end());
		imp.erase(remove(imp.begin(), imp.end(), '>'), imp.end());

		ss << imp;
		string name, variable, res;
		char ch;

		if (imp.substr(0, 2) == "</")
		{
			tag.pop_back();
		}
		else
		{
			ss >> ch >> name >> variable >> ch >> res;
			temp = "";
			if (tag.size() > 0)
			{
				temp = *tag.rbegin();
				temp = temp + "." + name;
			}
			else
			{
				temp = name;
			}
			tag.push_back(temp);
			inquer[count] = *tag.rbegin() + "~" + variable;
			result[count] = res;
			count++;
			while (!ss.eof())
			{
				ss >> variable >> ch >> res;
				inquer[count] = *tag.rbegin() + "~" + variable;
				result[count] = res;
				count++;
			}
		}
	}


	for (int i = 0; i < Q; i++)
	{
		int flag = 0, order = 0;;
		for (int j = 0; j < N; j++)
		{
			if (quer[i] == inquer[j])
			{
				flag = 1;
				order = j;
				break;
			}
		}
		if (flag == 1)
		{
			cout << result[order] << endl;
		}
		else
			cout << "Not Found!" << endl;
	}


	system("pause");
}