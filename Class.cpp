#include <iostream>
#include <sstream>
using namespace std;

class Student
{
private:
	string buff;
	stringstream ss;
	int years, standart;
	string name, surname;
public:
	void set_age(int age)
	{
		years = age;
	}
	void set_standard(int standard)
	{
		standart = standard;
	}
	void set_first_name(string first_name)
	{
		name = first_name;
	}
	void set_last_name(string last_name)
	{
		surname = last_name;
	}
	int get_age()
	{
		return years;
	}
	int get_standard()
	{
		return standart;
	}
	string get_first_name()
	{
		return name;
	}
	string get_last_name()
	{
		return surname;
	}
	string to_string()
	{
		ss << years << "," << name << "," << surname << "," << standart;
		ss >> buff;
		return buff;
	}
};

int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << endl;
	cout << st.get_last_name() << ", " << st.get_first_name() << endl;
	cout << st.get_standard() << endl;
	cout << endl;
	cout << st.to_string();

	return 0;
}
