#include <iostream>
#include <cstdio>
using namespace std;

int max_of_four(int a, int b, int c, int d);

int main() {
	int a, b, c, d;
	scanf_s("%d %d %d %d", &a, &b, &c, &d);
	int ans = max_of_four(a, b, c, d);
	printf("%d", ans);

	return 0;
}

int max_of_four(int a, int b, int c, int d)
{
	int greatest = a;

	if (greatest < b)
	{
		greatest = b;
	}
	if (greatest < c)
	{
		greatest = c;
	}
	if (greatest < d)
	{
		greatest = d;
	}
	return greatest;
}