#include<stdio.h>
#include<cmath>

void update(int *, int *);

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	scanf_s("%d %d", &a, &b);
	update(pa, pb);
	printf("%d\n%d", a, b);

	return 0;
}

void update(int *a, int *b)
{
	int tempa, tempb;
	tempa = *a;
	tempb = *b;
	*a = tempa + tempb;
	*b = abs(tempa - tempb);
}