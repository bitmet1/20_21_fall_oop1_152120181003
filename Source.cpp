#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>

using namespace std;

int num;
int SumFunc(int[]);
int MultFunc(int[]);
int SmallFunc(int[]);
int filetest(fstream &, string);
int EmptyTest(fstream & , string);
int CharTest(fstream &, string);
int IsequalTest(fstream &, string);

void main()
{
	int flag = 0;
	fstream data;
	string filename;
	int sum , mult , smallest;
	float average;

	///Getting Filename
	cout << "Acmak Istediginiz Dosya Adini Giriniz : ";
	cin >> filename;

	/// Open the File
	flag = filetest(data, filename);

	if (flag == 0)
	{
		data.open(filename, ios::in);

		data >> num;
		int *Number = new int[num];
		/// Get the number To Array
		for (int i = 0; i < num; i++)
		{
			data >> Number[i];
		}
		///Compution Functions Call
		sum = SumFunc(Number);
		mult = MultFunc(Number);
		smallest = SmallFunc(Number);
		/// All Displays
		float average = (float)sum / (float)num;
		cout << "Sum is : " << sum << endl;
		cout << "Product is :" << mult << endl;
		cout << "Average is : " << average << endl;
		cout << "Smallest is : " << smallest << endl << endl;

	}

	system("pause");
}

/**
 * This Function Calculates the sum of numbers
 * @param Number is a parameter
 */
int SumFunc(int Number[])
{
	int sum = 0;
	for (int i = 0; i < num; i++)
	{
		sum += Number[i];
	}
	return sum;
}

/**
 * This Function Calculates the multiplication of numbers
 * @param Number is a parameter
 */
int MultFunc(int Number[])
{
	int mult = 1;
	for (int i = 0; i < num; i++)
	{
		mult *= Number[i];
	}
	return mult;
}

/**
 * This Function Finds the smallest number in the file
 * @param Number is a parameter
 */
int SmallFunc(int Number[])
{
	int smallest = Number[0];
	for (int i = 0; i < num; i++)
	{
		if (Number[i] < smallest)
		{
			smallest = Number[i];
		}
	}
	return smallest;
}

/**
 * This function controls the file situation
 * @param data is a parameter
 * @param filename is a parameter
 */
int filetest(fstream &data, string filename)
{
	int flag = 0;
	data.open(filename, ios::in);
	if (data.is_open())
	{
		cout << endl << "File Was Opened!!" << endl << endl;
		data.close();
		flag = EmptyTest(data, filename);
		return flag;
	}
	else
	{
		cout << "File Was Not Opened" << endl;
		data.close();
		return 1;
	}
}

/**
 * This Function controls file is empty
 * @param data is a parameter
 * @param filename is a parameter
 */
int EmptyTest(fstream & data, string filename)
{
	int flag = 0;
	string test;
	// Dosya Kontrol
	data.open(filename , ios::in);
	while (!data.eof())
	{
		data >> test;
		if (test == "")
		{
			flag = 1;
		}
	}

	if (flag == 1)
	{
		cout << "ERROR !!! File is Empty!!" << endl << endl;
		return 1;
	}

	data.close();
	flag = CharTest(data, filename);
	return flag;
}

/**
 * This Function controls that is there any other characters except integers
 * @param data is a parameter
 * @param filename is a parameter
 */
int CharTest(fstream &data, string filename)
{
	char test;
	int flag = 0;
	data.open(filename, ios::in);
	while (!data.eof())
	{
		data >> test;
		if (!isdigit(test))
		{
			cout << "ERROR !!! Wrong Type Variable!!" << endl << endl;
			return 1;
		}
	}
	data.close();
	flag = IsequalTest(data, filename);
	return flag;
}

/**
 * This Function controls that the amount of numbers is correct
 * @param data is a parameter
 * @param filename is a parameter
 */
int IsequalTest(fstream &data, string filename)
{
	int count = 0, num ,temp;
	data.open(filename, ios::in);

	data >> num;
	while(data >> temp)
	{
		count++;
	}

	if (count != num)
	{
		cout << "ERROR !! The Amount of Numbers is Not Correct!!" << endl << endl;
		data.close();
		return 1;
	}
	data.close();
	return 0;
}