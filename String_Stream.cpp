#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str)
{
	vector <int> temp;
	int buff;
	char comm;
	stringstream ss;
	ss << str;
	while (!ss.eof())
	{
		ss >> buff;
		temp.push_back(buff);
		ss >> comm;
	}
	return temp;
}

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << endl;
	}

	return 0;
}
