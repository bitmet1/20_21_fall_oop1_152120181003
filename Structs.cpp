#include <iostream>
#include <string>

using namespace std;

struct Student
{
	int age;
	char name[51];
	char surname[51];
	int standart;
};

int main() {
	Student st;

	cin >> st.age;
	cin >> st.name;
	cin >> st.surname;
	cin >> st.standart;

	cout << st.age << " " << st.name << " " << st.surname << " " << st.standart;

	return 0;
}
