#include <vector>
#include <iostream>

using namespace std;

int main()
{
	vector <vector <int>> A;
	int N, q, k, i, j;

	cin >> N;
	cin >> q;

	A.resize(N);
	for (int i = 0; i < A.size(); i++)
	{
		cin >> k;
		A[i].resize(k);
		for (int j = 0; j < A[i].size(); j++)
		{
			cin >> A[i][j];
		}
	}

	int a = 0;
	while (a < q)
	{
		cin >> i >> j;
		cout << A[i][j] << endl;
		a++;
	}

	return 0;
}